﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MemberManagementSystem
{
    public partial class frmUserUpdate : Form
    {
        public frmUserUpdate()
        {
            InitializeComponent();
        }
     public   string son  = null;

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form3_Load(object sender, EventArgs e)
        {

            GetLevel();
            Etl();
        }


        public void GetLevel()
        {
            conn = new SqlConnection(str);
            try
            {
                DataSet ds = new DataSet();
                string sql = string.Format("  select* from  [MemberUserDB].[dbo].[Level]");
                SqlDataAdapter sda = new SqlDataAdapter(sql, conn);
                sda.Fill(ds, "Level");
                this.comboBox2.DataSource = ds.Tables["Level"];
                this.comboBox2.DisplayMember = "LevelName";
                this.comboBox2.ValueMember = "LevelNo";
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        string str = "Data Source=10.1.233.6;Initial Catalog=MemberUserDB;Persist Security Info=True;User ID=sa;Password=king520."; 
        SqlConnection conn = null;
        SqlCommand cmd;
        SqlDataReader read;
        /// <summary>
        /// 窗体加载事件方法
        /// </summary>
        public void Etl() {
            conn = new SqlConnection(str);
            try
            {
                conn.Open();
                string sql = string.Format("select	*  from [MemberUserDB].[dbo].[User] where UserNo = '{0}'", son);
                cmd = new SqlCommand(sql, conn);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    this.textBox1.Text = read["UserNo"].ToString();
                    this.textBox2.Text = read["PassWord"].ToString();
                    this.textBox3.Text = read["Name"].ToString();
                    this.textBox4.Text = read["Address"].ToString();
                    this.comboBox2.SelectedIndex = Convert.ToInt32(read["LevelType"])-1;
                    this.dateTimePicker1.Text = read["DueToDate"].ToString();
                    this.textBox7.Text = read["State"].ToString();
                }
                read.Close();
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
            finally {
                conn.Close();
            }
        }



        /// <summary>
        /// 单击关闭事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            qutry();
        }



        /// <summary>
        /// 关闭方法
        /// </summary>
        public void qutry() {
            this.Close();
        }



        /// <summary>
        /// 单击修改事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Modification();
        }



        /// <summary>
        /// 修改方法
        /// </summary>
        public void Modification() {
            conn = new SqlConnection(str);
            DateTime time = this.dateTimePicker1.Value;
            try
            {
                conn.Open();
                string sql = string.Format(@"select * from [MemberUserDB].[dbo].[User] 
                update [MemberUserDB].[dbo].[User] 
                set PassWord = '{0}',Name = '{1}',Address = '{2}',DueToDate = '{3}',LevelType='{5}'
                where UserNo = '{4}'", this.textBox2.Text, this.textBox3.Text, this.textBox4.Text, time, son,comboBox2.SelectedValue);
                cmd = new SqlCommand(sql, conn);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    MessageBox.Show("修改成功");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("修改失败");
                }
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
            finally {
                conn.Close();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void frmUserUpdate_FormClosing(object sender, FormClosingEventArgs e)
        {
  
        }
    }
}
