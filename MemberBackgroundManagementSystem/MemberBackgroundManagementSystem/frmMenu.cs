﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace MemberManagementSystem
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
            //Form
            this.Text = string.Empty;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {
            OpenChildForm(new frmUserManage());
        }


        //DragForm
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();// 鼠标捕获

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam); // 将消息发送给指定的窗口
        private void pTitle_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xF012, 0);
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void btnMaximize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                WindowState = FormWindowState.Normal;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //当前打开的Form
        private Form currentChildForm;

        /// <summary>
        /// 嵌入式打开form窗体方法
        /// </summary>
        /// <param name="childForm">窗体</param>
        private void OpenChildForm(Form childForm)
        {
            //当前窗体不等于空
            if (currentChildForm != null)
            {
                //只开放形式
                currentChildForm.Close();
            }
            
            currentChildForm = childForm;
            childForm.TopLevel = false;
            //设置form窗体边框为无边框
            childForm.FormBorderStyle = FormBorderStyle.None;
            //设置form为全部停靠
            childForm.Dock = DockStyle.Fill;
            //将form添加显示到控件
            pDesktop.Controls.Add(childForm);
            //控件数据对象设置为form
            pDesktop.Tag = childForm;
            childForm.BringToFront();
            //显示from
            childForm.Show();
        }

        private void btnUserManage_Click(object sender, EventArgs e)
        {
            OpenChildForm(new frmUserManage());
        }

        private void btnLoginLog_Click(object sender, EventArgs e)
        {
            OpenChildForm(new frmLoginLog());
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            frmUserAdd fr = new frmUserAdd();
            fr.ShowDialog();
        }
    }
}
